package com.company.readers;

import com.company.utils.Constants;
import com.company.utils.RegexParser;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    private File file;

    public FileReader(String pathname) {
        file = new File(pathname);
    }

    public int[][] read() {

        try {
            Scanner in = new Scanner(file);

            Point dimen = RegexParser.getDimentions(in.nextLine());
            int[][] array = new int[dimen.x][dimen.y];
            int i = 0;
            while (in.hasNext()) {
                putLineIntoArray(in.nextLine().toCharArray(), i, array);
                i++;
            }

            in.close();
            return array;
        } catch (FileNotFoundException e) {
            System.out.println("Файл відсутній");
        }
        return null;
    }

    private void putLineIntoArray(char[] mas, int linePos, int[][] array) {
        for (int i = 0; i < mas.length; i++) {
            array[linePos][i] = getType(mas[i]);
        }
    }

    private int getType(char c) {
        if (c == '#') {
            return Constants.TYPE_PAINT;
        } else {
            return Constants.TYPE_CLEAR;
        }
    }

}
