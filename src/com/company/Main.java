package com.company;

import com.company.readers.FileReader;
import com.company.utils.MatrixUtils;

import java.awt.*;
import java.util.Random;

public class Main {

    private int[] mas = new int[10];
    private Random random;


    public static final String TAG = "painter_tag";


    public static final boolean showLog = false;

    public static void main(String[] args) {

        int[][] mArray;
        int[][] mResult = new int[1000000][4];
        int currentCommand = 0;
        Point mDimentions = new Point();

        // write your code here
        System.out.println("Hello World!");
        long startTime = System.currentTimeMillis();
        System.out.println(System.currentTimeMillis());
        FileReader reader = new FileReader("input.in");
        mArray = reader.read();

        int[] coords = MatrixUtils.getLongestLine(mArray);
        while (coords != null) {
//            Log.d(TAG, "" + currentCommand);
            MatrixUtils.writeCoords(coords, mResult, currentCommand);
            MatrixUtils.paintMatrix(mArray, coords);
            if (showLog) {
                MatrixUtils.writeMatrix(mArray);
            }
            coords = MatrixUtils.getLongestLine(mArray);
//                    Log.d(TAG, "PAINT_LINE " + coords[1] + " " + coords[0] + " " + coords[3] + " " + coords[2]);
            currentCommand++;
        }
        MatrixUtils.outputCoords(mResult, currentCommand);
        System.out.print(System.currentTimeMillis() - startTime);
        System.out.print("\n" + currentCommand);

    }

}
