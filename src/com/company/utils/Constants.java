package com.company.utils;

/**
 * Created by mpodolsky on 10.02.2016.
 */
public interface Constants {

    int TYPE_CLEAR = 0;
    int TYPE_PAINT = 1;
    int TYPE_PAINTED = 2;

}
