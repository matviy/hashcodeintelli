package com.company.utils;



import com.company.Main;
import com.company.writers.Writer;

/**
 * Created by mpodolsky on 27.10.2015.
 */
public class MatrixUtils {

    public static void writeMatrix(int[][] matrix) {
        if (matrix != null) {
            String ms = "matrix:\n";
            for (int j = 0; j < matrix.length; j++) {
                int height = matrix[j].length;
                for (int k = 0; k < height; k++) {
                    ms = ms + " " + makeStringCell(matrix[j][k]);
                }
                ms = ms + "\n";
            }
            System.out.print(ms);
//            Log.d(tag, ms);
        }
    }

    private static String makeStringCell(int f) {
        String s = "";
        String fstring = null;
        if (f == Constants.TYPE_CLEAR) {
            fstring = "-";
        } else if (f == Constants.TYPE_PAINT) {
            fstring = "0";
        } else if (f == Constants.TYPE_PAINTED) {
            fstring = "#";
        } else {
            fstring = String.valueOf(f);
        }
        for (int i = 0; i < 1 - fstring.length(); i++) {
            s = s + " ";
        }
        s = s + fstring;
        return s;
    }

    public static int[] getLongestLine(int[][] mas) {
        int[] coords = {0,0,0,0};
        int maxLength = 0;
        int currentLength = 0;
        int startX = 0;
        int startY = 0;
        int currentX = 0;
        int currentY = 0;
        boolean isFound = false;
        int height = mas.length;
        int width = mas[0].length;
        boolean wasFound = false;

        //horizontal
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (!isFound) {
                    if (mas[i][j] == Constants.TYPE_PAINT) {
                        wasFound = true;
                        startX = j;
                        startY = i;
                        currentX = startX;
                        currentY = startY;
                        isFound = true;
                        currentLength = 1;

                        if (j == width - 1) {
                            if (currentLength > maxLength) {
                                coords[0] = startX;
                                coords[1] = startY;
                                coords[2] = currentX;
                                coords[3] = currentY;
                                maxLength = currentLength;
                            }
                            isFound = false;
                        }

                    }
                } else {
                    if (mas[i][j] == Constants.TYPE_PAINT) {
//                        currentLength++;
                        currentX = j;
                        currentY = i;
                    }
                    if (mas[i][j] == Constants.TYPE_CLEAR || j == width - 1) {
                        currentLength = currentX - startX + 1;
                        if (currentLength > maxLength) {
                            coords[0] = startX;
                            coords[1] = startY;
                            coords[2] = currentX;
                            coords[3] = currentY;
                            maxLength = currentLength;
                        }
                        isFound = false;
//                        currentLength = 0;
                    }
                }
            }
        }

        currentLength = 0;
        startX = 0;
        startY = 0;
        currentX = 0;
        currentY = 0;
        isFound = false;
        height = mas.length;
        width = mas[0].length;
        //vertical
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < height; i++) {
                if (!isFound) {
                    if (mas[i][j] == Constants.TYPE_PAINT) {
                        startX = j;
                        startY = i;
                        currentX = startX;
                        currentY = startY;
                        isFound = true;
                        currentLength = 1;
                    }

                    if (i == height - 1) {
                        if (currentLength > maxLength) {
                            coords[0] = startX;
                            coords[1] = startY;
                            coords[2] = currentX;
                            coords[3] = currentY;
                            maxLength = currentLength;
                        }
                        isFound = false;
                        currentLength = 0;
                    }

                } else {
                    if (mas[i][j] == Constants.TYPE_PAINT) {
//                        currentLength++;
                        currentX = j;
                        currentY = i;
                    }
                    if (mas[i][j] == Constants.TYPE_CLEAR || i == height - 1) {
                        currentLength = currentY - startY + 1;
                        if (currentLength > maxLength) {
                            coords[0] = startX;
                            coords[1] = startY;
                            coords[2] = currentX;
                            coords[3] = currentY;
                            maxLength = currentLength;
                        }
                        isFound = false;
                        currentLength = 0;
                    }
                }
            }
        }

        if (wasFound) {
            if (Main.showLog) {
                System.out.print("max: " + maxLength);
//                Log.d(MainActivity.TAG, "max: " + maxLength);
            }
            return coords;
        } else {
            return null;
        }
    }

    public static void paintMatrix(int[][] mas, int[] coords) {
        int x2 = coords[2];
        int y2 = coords[3];
        for (int x1 = coords[0]; x1 <= x2; x1++) {
            for (int y1 = coords[1]; y1 <= y2; y1++) {
                mas[y1][x1] = Constants.TYPE_PAINTED;
            }
        }
    }

    public static void writeCoords(int[] coords, int[][] res, int pos) {
        res[pos][0] = coords[0];
        res[pos][1] = coords[1];
        res[pos][2] = coords[2];
        res[pos][3] = coords[3];
        if (Main.showLog) {
            System.out.print("line: " + coords[1] + " " + coords[0] + " " + coords[3] + " " + coords[2]);
//            Log.d(Main.TAG, "line: " + coords[1] + " " + coords[0] + " " + coords[3] + " " + coords[2]);
        }
    }

    public static String outputCoords(int[][] coords, int size) {
        String s = "";
        s = s + String.valueOf(size) + "\n";
//        System.out.print(s);
//        Log.d(tag, String.valueOf(size));
        for (int i = 0; i < size; i++) {
            s = s + "PAINT_LINE " + coords[i][1] + " " + coords[i][0] + " " + coords[i][3] + " " + coords[i][2] + "\n";
//            Log.d(tag, "PAINT_LINE " + coords[i][1] + " " + coords[i][0] + " " + coords[i][3] + " " + coords[i][2]);
        }
//        if (Main.showLog) {
//            System.out.print(s);
//        }
        Writer writer = new Writer();
        writer.write(s);
        return s;
    }

}
