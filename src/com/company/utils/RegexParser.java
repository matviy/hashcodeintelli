package com.company.utils;



import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexParser {
    private static String REGEX_DIMENTIONS = "(\\d+)\\s+(\\d+)";

    public static Point getDimentions(String line) {
        Matcher m = Pattern.compile(REGEX_DIMENTIONS).matcher(line.toLowerCase());
        Point p = new Point();
        if (m.find()) {
            p.setLocation(Integer.valueOf(m.group(1)), Integer.valueOf(m.group(2)));
        }
        return p;
    }

}
